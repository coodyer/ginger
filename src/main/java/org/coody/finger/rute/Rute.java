package org.coody.finger.rute;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.coody.finger.util.FileUtil;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;

public class Rute {

	public static void main(String[] args) throws IllegalAccessException, IllegalArgumentException,
			InvocationTargetException, UnsupportedEncodingException {
		if (args == null || args.length < 1) {
			System.err.println("请输入指令");
			return;
		}
		String command = args[0];
		Rute rute = new Rute();
		Method methods[] = Rute.class.getDeclaredMethods();
		for (Method method : methods) {
			if (!command.equals(method.getName())) {
				continue;
			}
			method.invoke(rute, (Object) args);
		}

	}

	/**
	 * 扫描文件指纹
	 * 
	 * @param args 0指令 1扫描地址 2指纹保存地址
	 * @throws UnsupportedEncodingException
	 */
	public void scanner(String[] args) throws UnsupportedEncodingException {
		if (args.length < 2) {
			System.err.println("请输入扫描地址");
			return;
		}
		String path = FileUtil.formatUrl(args[1]);
		String save = "finger.txt";
		if (args.length > 2) {
			save = args[2];
		}
		List<File> files = FileUtil.getFiles(path);
		Map<String, String> fingers = new HashMap<String, String>();
		for (File file : files) {
			if (file.isDirectory()) {
				continue;
			}
			String md5 = FileUtil.getMD5(file);
			String key = FileUtil.formatUrl(file.getPath()).replace(path, "");
			fingers.put(key, md5);
		}
		FileUtil.write(save, JSON.toJSONString(fingers));
	}

	/**
	 * 整理增量包
	 * 
	 * @param args 0指令 1扫描地址 2已部署文件指纹地址
	 */
	public void contraster(String[] args) {
		if (args.length < 2) {
			System.err.println("请输入扫描地址（0指令 1扫描地址 2已部署文件指纹地址）");
			return;
		}
		if (args.length < 3) {
			System.err.println("请输入指纹文件");
			return;
		}
		String path = FileUtil.formatUrl(args[1]);
		String finger = args[2];
		Map<String, String> fingers = JSON.parseObject(FileUtil.readFile(finger),
				new TypeReference<Map<String, String>>() {
				});
		if (fingers == null) {
			fingers = new HashMap<String, String>();
		}
		Map<String, String> localfingers = new HashMap<String, String>();
		List<File> files = new ArrayList<File>();
		List<File> localFiles = FileUtil.getFiles(path);
		for (File file : localFiles) {
			if (file.isDirectory()) {
				continue;
			}
			String key = FileUtil.formatUrl(file.getPath()).replace(path, "");
			String md5 = FileUtil.getMD5(file);
			localfingers.put(key, md5);
			if (!fingers.containsKey(key)) {
				System.out.println("新增文件>>" + key);
				files.add(file);
				continue;
			}
			if (!md5.equals(fingers.get(key))) {
				System.out.println("修改文件>>" + key);
				files.add(file);
				continue;
			}
			file.delete();
		}
		// 清理空文件
		forParent: for (File file : localFiles) {
			if (!file.isDirectory()) {
				continue;
			}
			List<File> childFiles = FileUtil.getFiles(file.getPath());
			if (childFiles == null || childFiles.isEmpty()) {
				FileUtil.delete(file.getPath());
				continue;
			}
			for (File child : childFiles) {
				if (!child.isDirectory()) {
					continue forParent;
				}
			}
			if (files.contains(file)) {
				continue;
			}
			FileUtil.delete(file.getPath());
			continue;
		}
		FileUtil.write(path + "/" + "finger.txt", JSON.toJSONString(localfingers));
	}

	/**
	 * 清理多余文件
	 * 
	 * @param args 0指令 1已部署地址
	 */
	public void clear(String[] args) {
		if (args.length < 2) {
			System.err.println("请输入扫描地址（0指令 1已部署地址）");
			return;
		}
		String path = FileUtil.formatUrl(args[1]);
		Map<String, String> fingers = JSON.parseObject(FileUtil.readFile(path + "/" + "finger.txt"),
				new TypeReference<Map<String, String>>() {
				});
		List<File> files = FileUtil.getFiles(path);
		for (File file : files) {
			if (file.isDirectory()) {
				continue;
			}
			String key = FileUtil.formatUrl(file.getPath()).replace(path, "");
			if (fingers.containsKey(key)) {
				continue;
			}
			System.out.println("删除文件>>" + key);
			file.delete();
		}
		FileUtil.delete(path + "/" + "finger.txt");
	}

}
